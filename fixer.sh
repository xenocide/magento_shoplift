#!/bin/bash

####################################################################
##
## Automatically patch many magento installations
##
## Written for ISPConfig3 but should work on any control panel installation
## 
## Read instructions at: https://www.howtoforge.com/community/threads/magento-shoplift-mass-update-script.69818/
##
## By Mark Pugh @ WebCreationUK
## 
####################################################################


## TO GENERATE paths.txt run > 
# find /var/www/ -wholename '*/app/code/core/Mage/Core/Controller/Request/Http.php' | xargs grep -L _internallyForwarded > paths.txt


# Get the file list
while read p; do
	echo "--------------------"
	path_to_http=$p
	replace=""
	# The root path of the mage install
	root_path="${p///app\/code\/core\/Mage\/Core\/Controller\/Request\/Http.php/}"
	#echo $root_path
	# The version number of the magento install so we know which patch to apply
	version=`cd $root_path && php -r "require 'app/Mage.php'; echo Mage::getVersion();"`
	echo $version
	naked_version="${version//\./}"
	#echo $naked_version
	# Which patch do we want to apply?
	if [ $naked_version -lt 1500 -a $naked_version -ge 1400 ]; then
		filename="1.4.0.x"
	elif [ $naked_version -ge 1510 -a $naked_version -lt 1600 ]; then
		filename="1.5.1.x"
	elif [ $naked_version -ge 1600 -a $naked_version -lt 1610 ]; then
		filename="1.6.0.x"
	elif [ $naked_version -ge 1610 -a $naked_version -le 1620 ]; then
		filename="1.6.1.x"
	elif [ $naked_version -ge 1700 -a $naked_version -lt 1800 ]; then
		filename="1.7.x.x"
	elif [ $naked_version -ge 1800 ]; then
		filename="1.8.x.x"
	else
		filename="version"
	fi;

	# make sure we actually figured out which patch we want to play with
	if [ $filename != "version" ]; then
		# The file we're actually going to run
		patch_file="$filename/patch-shoplift.sh"
		echo $patch_file
		# Let's just make a note of the owner of the files
		file_owner=`ls -ld $root_path | awk '{print $3}'`
		file_group=`ls -ld $root_path | awk '{print $4}'`
		#echo $file_owner
		#echo $file_group
		# Actually run the patch
		fef=`cd $root_path && cp ~/magento_shoplift/$patch_file . && chmod +x patch-shoplift.sh && ./patch-shoplift.sh && rm -f patch-shoplift.sh`
		# Reset the file owners
		fef2=`chown -Rv $file_owner:$file_group $root_path/app/code/core/`
		#echo $fef
		#echo $fef2
		echo "$root_path patched ($filename applied)"
	fi;

done <~/magento_shoplift/paths.txt
# Now clear the magento caches
#find /var/www/ -wholename '*/var/cache' -exec echo {}/* \;
