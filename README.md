# README #

A script for updating multiple magento installations for the shoplift vulnerability.

### Follow the instructions on howtoforge! ###
[https://www.howtoforge.com/community/threads/magento-shoplift-mass-update-script.69818/](https://www.howtoforge.com/community/threads/magento-shoplift-mass-update-script.69818/)
